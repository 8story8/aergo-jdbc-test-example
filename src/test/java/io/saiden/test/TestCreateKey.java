package io.saiden.test;

import hera.api.model.EncryptedPrivateKey;
import hera.key.AergoKey;
import hera.key.AergoKeyGenerator;


public class TestCreateKey {
  public static void main(String[] args) {
    for(int i = 0; i < 6; i++) {
      AergoKey aergoKey = new AergoKeyGenerator().create();
      EncryptedPrivateKey encryptedKey = aergoKey.export("admin");
      System.out.println("Private Key : " + encryptedKey.getEncoded());
      System.out.println("Address : " + aergoKey.getAddress().getEncoded());
    }
  }
}
