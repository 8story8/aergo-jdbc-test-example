function version()
	return "1.0.0"
end

function exec(sql, ...)
	local stmt = db.prepare(sql)
	count = stmt:exec(...)
	return count
end

function query_begin(sql, ...)
    local stmt = db.prepare(sql)
    local rs = stmt:query(...)
    local r = {}
    local colcnt = rs:colcnt()
    local colmetas = stmt:column_info()
    while rs:next() do
        local k = {rs:get()}
        for i = 1, colcnt do
            if k[i] == nil then
                k[i] = {}
            end
        end
        table.insert(r, k)
    end
    if (#r == 0) then
        return {colcnt=colcnt, rowcnt=0 , rows=nil, colmetas=colmetas}
    end

    return {colcnt=colcnt, rowcnt=#r, rows=r, colmetas=colmetas, snapshot=db.getsnap()}
end

function query_next(snap, sql, ...)
	db.open_with_snapshot(snap)
	local rs = db.query(sql, ...)
	local r = {}
	local colcnt = rs:colcnt()
	while rs:next() do
		local k = {rs:get()}
		for i = 1, colcnt do
			if k[i] == nil then
				k[i] = {}
			end
		end
		table.insert(r, k)
	end
	if (#r == 0) then
		return {colcnt=colcnt, rowcnt=0 , rows=nil}
	end

	return {colcnt=colcnt, rowcnt=#r, rows=r}
end

function getmeta(sql)
	local stmt = db.prepare(sql)
	return {colmetas=stmt:column_info(), bindcnt=stmt:bind_param_cnt()}
end

-- 사용자 가입
function register_user(user_address, user_name)
	if system.getSender() ~= system.getCreator() then
	  return
	end
  db.exec("insert into USER (USER_ADDRESS, USER_NAME, USER_ASSET, CREATED_AT) values (?, ?, ?, ?)", user_address, user_name, get_user_registration_asset(), system.getTimestamp())
end

-- 미터기 등록
function register_device(type_name, owner_address, device_address)
  if system.getSender() ~= system.getCreator() then
	  return
  end
	local type = find_type_by_name(type_name)
	if type == nil then
	  return false
	end
  local stmt = db.prepare("insert into DEVICE (DEVICE_ADDRESS, TYPE_ID, USER_ADDRESS, CREATED_AT) values (?, ?, ?, ?)")
  stmt:exec(device_address, type.id, owner_address, system.getTimestamp())
end

-- 미터기 데이터 등록
function register_device_data(device_address, device_data)
	db.exec("insert into DEVICE_DATA (DEVICE_ADDRESS, DEVICE_DATA, CREATED_AT) values (?, ?, ?, ?)", device_address, device_data, system.getTimestamp())
end

-- 구독 요청
function request_subscription(applicant_address, approver_address, device_address)
  if system.getSender() ~= system.getCreator() then
	  return
  end
	local plan = find_plan_by_device_address(device_address)
	if plan == nil then
		return false
	end
  local subscription_status = "PENDING"
	db.exec("insert into SUBSCRIPTION (APPLICANT_ADDRESS, APPROVER_ADDRESS, DEVICE_ADDRESS, SUBSCRIPTION_STATUS, PLAN_ID) values (?, ?, ?, ?, ?)", applicant_address, approver_address, device_address, subscription_status, plan.id)
end

-- 구독 취소
function cancel_subscription(applicant_address, approver_address, device_address)
  if system.getSender() ~= system.getCreator() then
	  return
  end
  local subscription_status = "PENDING"
	db.exec("delete from SUBSCRIPTION where APPLICANT_ADDRESS=? and APPROVER_ADDRESS=? and DEVICE_ADDRESS=? and SUBSCRIPTION_STATUS=?", applicant_address, approver_address, device_address, subscription_status)
end

-- 구독 승인
function approve_subscription(applicant_address, approver_address, device_address)
  if system.getSender() ~= system.getCreator() then
	  return
  end
  local prev_subscription_status = "PENDING"
  local subscription_status = "APPROVED"
	local plan = find_plan_by_device_address(device_address)
	if plan == nil then
		return false
	end
	local started_at = system.getTimestamp()
	local ended_at = system.getTimestamp() + (86400 * plan.period)
	db.exec("update SUBSCRIPTION set STARTED_AT=?, ENDED_AT=?, SUBSCRIPTION_STATUS=?, TRANSACTION_ID=? where APPLICANT_ADDRESS=? and APPROVER_ADDRESS=? and DEVICE_ADDRESS=? and SUBSCRIPTION_STATUS=?", started_at, ended_at, subscription_status, system.getTxhash(), applicant_address, approver_address, device_address, prev_subscription_status)
	update_user_asset_by_subscription("APPLICANT", applicant_address, plan)
	update_user_asset_by_subscription("APPROVER", approver_address, plan)
end

function constructor()
  if exists_deployment() then
	  return
	end
	init_table()
	init_config()
end

-- 스마트 컨트랙트 배포 여부 확인
function exists_deployment()
	local table = { "DEVICE", "DEVICE_DATA", "PLAN", "SUBSCRIPTION", "TYPE", "USER" }
	local count_table = 0
	for i = 1, #table do
	  if exists_table(table[i]) then
		  count_table = count_table+1
		end
	end

	if count_table == #table then
	  return true
	else
	  return false
	end
end

-- 스마트 컨트랙트 테이블 존재 여부 확인
function exists_table(table_name)
  local rs = db.query("select count(*) from sqlite_master where type = ? and name = ?", "table", table_name)
  while rs:next() do
	  local count = rs:get()
	  if rs:get() > 0 then
		  return true
	  else
		  return false
		end
	end
end

-- 스마트 컨트랙트 테이블 생성
function init_table()
 db.exec("create table DEVICE (DEVICE_ADDRESS varchar not null, CREATED_AT bigint, TYPE_ID integer, USER_ADDRESS varchar, primary key (DEVICE_ADDRESS))")
 db.exec("create table DEVICE_DATA (DEVICE_DATA_ID integer primary key autoincrement, DEVICE_DATA varchar, CREATED_AT bigint, DEVICE_ADDRESS varchar)")
 db.exec("create table PLAN (PLAN_ID integer primary key autoincrement, PLAN_ASSET numeric, PLAN_PERIOD integer, PLAN_REWARD numeric)")
 db.exec("create table TYPE (TYPE_ID integer primary key autoincrement, TYPE_NAME varchar, PLAN_ID integer)")
 db.exec("create table SUBSCRIPTION (SUBSCRIPTION_ID integer primary key autoincrement, APPROVER_ADDRESS varchar, ENDED_AT bigint, STARTED_AT bigint, SUBSCRIPTION_STATUS varchar, DEVICE_ADDRESS varchar, PLAN_ID integer, APPLICANT_ADDRESS varchar, TRANSACTION_ID varchar)")
 db.exec("create table USER (USER_ADDRESS varchar not null, USER_NAME varchar, USER_ASSET numeric, CREATED_AT bigint, primary key (USER_ADDRESS))")
end

-- 스마트 컨트랙트 테이블 데이터 초기화
function init_table_data()
  if system.getSender() ~= system.getCreator() then
	  return
  end
	db.exec("delete from DEVICE")
	db.exec("delete from DEVICE_DATA")
	db.exec("delete from PLAN")
	db.exec("delete from SUBSCRIPTION")
	db.exec("delete from TYPE")
	db.exec("delete from USER")
	init_config()
end

-- 스마트 컨트랙트 테이블 데이터 설정 초기화
function init_config()
	insert_plan_by_type_name("전력", 100, 10, 50)
	insert_plan_by_type_name("습도", 200, 20, 100)
	insert_plan_by_type_name("온도", 300, 30, 500)
	set_user_registration_asset(10000)
end

-- 사용자 주소에 따른 사용자 정보 반환
function find_user_by_address(user_address)
  local rs = db.query("select USER_ADDRESS, USER_ASSET, CREATED_AT from USER where USER_ADDRESS=?", user_address)
  while rs:next() do
	  local col1, col2, col3 = rs:get()
	  local user = {
		  address = col1,
		  asset = col2,
		  created_at = col3
	  }
	  return user
  end
end

-- 미터기 주소에 따른 구독 정책 반환
function find_plan_by_device_address(device_address)
  local type_rs = db.query("select TYPE_ID from DEVICE where DEVICE_ADDRESS=?", device_address)
  while type_rs:next() do
	  local type_id = type_rs:get()
		local plan_rs = db.query("select PLAN_ID from TYPE where TYPE_ID=?", type_id)
		while plan_rs:next() do
		  local plan_id = plan_rs:get()
			local rs = db.query("select PLAN_ID, PLAN_ASSET, PLAN_PERIOD, PLAN_REWARD from PLAN where PLAN_ID=?", plan_id)
			while rs:next() do
			  local col1, col2, col3, col4 = rs:get()
				local plan = {
					id = col1,
					asset = col2,
					period = col3,
					reward = col4
				}
				return plan
			end
		end
  end
end

-- 구독에 따른 사용자 자산 정산
function update_user_asset_by_subscription(user_type, user_address, plan)
  local user = find_user_by_address(user_address)
  if user_type == "APPLICANT" then
	  local applicant_asset = user.asset - plan.asset + plan.reward
    db.exec("update USER set USER_ASSET=? where USER_ADDRESS=?", applicant_asset, user_address)
	elseif user_type == "APPROVER" then
	  local approver_asset = user.asset + plan.asset - plan.reward
		db.exec("update USER set USER_ASSET=? where USER_ADDRESS=?", approver_asset, user_address)
	end
end

-- 미터기 유형명에 따른 미터기 유형 반환
function find_type_by_name(type_name)
  local rs = db.query("select TYPE_ID, TYPE_NAME, PLAN_ID from TYPE where TYPE_NAME=?", type_name)
	while rs:next() do
	  local col1, col2, col3 = rs:get()
		local type = {
			id = col1,
			name = col2,
			plan_id = col3
		}
		return type
	end
end

-- 미터기 유형명에 따른 구독 정책 삽입
function insert_plan_by_type_name(type_name, plan_asset, plan_period, plan_reward)
  if system.getSender() ~= system.getCreator() then
	  return
  end
	db.exec("insert into PLAN (PLAN_ASSET, PLAN_PERIOD, PLAN_REWARD) values (?, ?, ?)", plan_asset, plan_period, plan_reward)
	local rs = db.query("select PLAN_ID from PLAN order by PLAN_ID desc limit 1")
	while rs:next() do
	  local plan_id = rs:get()
    db.exec("insert into TYPE (TYPE_NAME, PLAN_ID) values (?, ?)", type_name, plan_id)
	end
end

-- 미터기 유형명에 따른 구독 정책 변경
function update_plan_by_type_name(type_name, plan_asset, plan_period, plan_reward)
  if system.getSender() ~= system.getCreator() then
	  return
  end
	local rs = db.query("select PLAN_ID from TYPE where TYPE_NAME=?", type_name)
	while rs:next() do
		local plan_id = rs:get()
		db.exec("update PLAN set PLAN_ASSET=?, PLAN_PERIOD=?, PLAN_REWARD=? where PLAN_ID=?", plan_asset, plan_period, plan_reward, plan_id)
	end
end

-- 사용자 가입 시 무료로 충전해주는 사용자 자산 설정
function set_user_registration_asset(asset)
  if system.getSender() ~= system.getCreator() then
	  return
  end
	system.setItem("asset", asset)
end

-- 사용자 가입 시 무료로 충전해주는 사용자 자산 반환
function get_user_registration_asset()
  return system.getItem("asset")
end

abi.register(exec, set_user_registration_asset, register_user, register_device, register_device_data, request_subscription, cancel_subscription, approve_subscription, init_table_data, insert_plan_by_type_name, update_plan_by_type_name)
abi.register_view(version, query_begin, query_next, getmeta)
