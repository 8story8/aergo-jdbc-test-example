package io.saiden.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import io.saiden.bc.dto.AvailableSubscription;
import io.saiden.bc.dto.CompletedSubscription;
import io.saiden.bc.dto.DeviceAndUser;
import io.saiden.bc.dto.PendingSubscription;
import io.saiden.bc.dto.ToBeCompletedSubscription;
import io.saiden.bc.model.ResultMsg;
import io.saiden.bc.model.User;
import io.saiden.bc.service.UserService;

@RestController
@RequestMapping("api")
public class ApiController {

  @Autowired
  private UserService userService;

  // 모바일
  // 사용자 등록 화면
  @PostMapping("/users")
  public boolean registerUser(@RequestParam("address") String address /* txForm */) {
    boolean flag = userService.registerUserConditional(address);

    if(flag) {
      System.out.println("가입 트랜잭션 실행");
      return true;
    }

    return false;
  }

  // 사용자 관리 화면
  @GetMapping("/users/{address}")
  public User findUserByUserAddress(@PathVariable("address") String address) {
    return userService.findUserByUserAddress(address);
  }

  // 구독 관리 화면
  @GetMapping("/users/{address}/com-sub/pages/{page-num}")
  public ResultMsg findCompletedSubscriptionsByUserAddress(@PathVariable("address") String address,
      @PathVariable("page-num") Integer pageNum) {
    Page<CompletedSubscription> pages =
        userService.findCompletedSubscriptionsByUserAddress(address, pageNum);

    if (pages.isEmpty()) {
      return ResultMsg.of(pages, false);
    }

    return ResultMsg.of(pages);
  }

  // 구독 승인 화면
  @GetMapping("/users/{address}/to-be-com-sub/pages/{page-num}")
  public ResultMsg findToBeCompletedSubscriptionsByUserAddress(
      @PathVariable("address") String address, @PathVariable("page-num") Integer pageNum) {
    Page<ToBeCompletedSubscription> pages =
        userService.findToBeCompletedSubscriptionsByUserAddress(address, pageNum);

    if (pages.isEmpty()) {
      return ResultMsg.of(pages, false);
    }

    return ResultMsg.of(pages);
  }

  // 구독 요청 중 화면
  @GetMapping("/users/{address}/pen-sub/pages/{page-num}")
  public ResultMsg findPendingSubscriptionsByUserAddress(@PathVariable("address") String address,
      @PathVariable("page-num") Integer pageNum) {
    Page<PendingSubscription> pages =
        userService.findPendingSubscriptionsByUserAddress(address, pageNum);

    if (pages.isEmpty()) {
      return ResultMsg.of(pages, false);
    }

    return ResultMsg.of(pages);
  }

  // 구독 요청 화면
  @GetMapping("/users/{address}/avail-sub/pages/{page-num}")
  public ResultMsg findAvailableSubscriptionsByUserAddress(@PathVariable("address") String address,
      @PathVariable("page-num") Integer pageNum) {
    Page<AvailableSubscription> pages =
        userService.findAvailableSubscriptionsByUserAddress(address, pageNum);

    if (pages.isEmpty()) {
      return ResultMsg.of(pages, false);
    }

    return ResultMsg.of(pages);
  }

  @GetMapping("/search/users/{address}/name/{name}/avail-sub/pages/{page-num}")
  public ResultMsg filterAvailableSubscriptionsByUserName(@PathVariable("address") String address, @PathVariable("name") String name,
      @PathVariable("page-num") Integer pageNum) {
    Page<AvailableSubscription> pages =
        userService.filterAvailableSubscriptionsByUserName(address, name, pageNum);
    
    if (pages.isEmpty()) {
      return ResultMsg.of(pages, false);
    }

    return ResultMsg.of(pages);
  }

  // 미터기 리스트
  @GetMapping("/users/{address}/devices/pages/{page-num}")
  public ResultMsg findDevicesByUserAddress(@PathVariable("address") String address,
      @PathVariable("page-num") Integer pageNum) {
    Page<DeviceAndUser> pages = userService.findDevicesByUserAddress(address, pageNum);
    
    if (pages.isEmpty()) {
      return ResultMsg.of(pages, false);
    }

    return ResultMsg.of(pages);
  }

  // 등록 가능한 미터기 리스트



  // 웹
}
