package io.saiden.bc.dto;

import java.math.BigDecimal;
import io.saiden.bc.model.Subscription;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class ToBeCompletedSubscription {

  @Getter
  @Setter
  private String deviceAddress;

  @Getter
  @Setter
  private String applicantName;

  @Getter
  @Setter
  private String applicantAddress;

  @Getter
  @Setter
  private BigDecimal asset;

  @Getter
  @Setter
  private BigDecimal reward;
  
  @Getter
  @Setter
  private Long createdAt;


  public static ToBeCompletedSubscription fromEntity(Subscription subscription) {
    return new ToBeCompletedSubscription(subscription.getDevice().getAddress(),
        subscription.getUser().getName(), subscription.getUser().getAddress(),
        subscription.getPlan().getAsset(), subscription.getPlan().getReward(), subscription.getDevice().getCreatedAt());
  }
}
