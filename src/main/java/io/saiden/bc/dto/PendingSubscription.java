package io.saiden.bc.dto;

import java.math.BigDecimal;
import io.saiden.bc.model.Subscription;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class PendingSubscription {

  @Getter
  @Setter
  private String deviceAddress;

  @Getter
  @Setter
  private String approverName;

  @Getter
  @Setter
  private String approverAddress;

  @Getter
  @Setter
  private BigDecimal asset;

  @Getter
  @Setter
  private BigDecimal reward;
  
  @Getter
  @Setter
  private Long createdAt;
  
  public static PendingSubscription fromEntity(Subscription subscription) {
    return new PendingSubscription(subscription.getDevice().getAddress(),
        subscription.getDevice().getUser().getAddress(),
        subscription.getDevice().getUser().getName(), subscription.getPlan().getAsset(),
        subscription.getPlan().getReward(), subscription.getDevice().getCreatedAt());
  }
}
