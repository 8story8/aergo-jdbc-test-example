package io.saiden.bc.dto;

import io.saiden.bc.model.Subscription;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class CompletedSubscription {

  @Getter
  @Setter
  private String deviceAddress;
  
  @Getter
  @Setter
  private Long startedAt;
  
  @Getter
  @Setter
  private Long endedAt;
  
  @Getter
  @Setter
  private Long createdAt;
  
  public static CompletedSubscription fromEntity(Subscription subscription) {
    return new CompletedSubscription(subscription.getDevice().getAddress(), subscription.getStartedAt(), subscription.getEndedAt(), subscription.getDevice().getCreatedAt());
  }
}
