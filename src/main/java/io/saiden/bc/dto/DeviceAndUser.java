package io.saiden.bc.dto;

import java.math.BigDecimal;
import io.saiden.bc.model.Device;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
public class DeviceAndUser {

  @Getter
  @Setter
  private String deviceAddress;

  @Getter
  @Setter
  private String userAddress;

  @Getter
  @Setter
  private BigDecimal asset;

  @Getter
  @Setter
  private BigDecimal reward;
  
  @Getter
  @Setter
  private Long createdAt;


  public static DeviceAndUser fromEntity(Device device) {
    return new DeviceAndUser(device.getAddress(), device.getUser().getAddress(), device.getType().getPlan().getAsset(), device.getType().getPlan().getReward(), device.getCreatedAt());
  }
}
