package io.saiden.bc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import io.saiden.bc.model.DeviceData;

@Repository
public interface DeviceDataRepository extends JpaRepository<DeviceData, Integer>{
}
