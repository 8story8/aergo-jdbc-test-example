package io.saiden.bc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import io.saiden.bc.model.Type;

@Repository
public interface TypeRepository extends JpaRepository<Type, Integer>{
  Long findIdByName(String name);
  
}
