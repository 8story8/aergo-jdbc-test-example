package io.saiden.bc.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import io.saiden.bc.model.Subscription;
import io.saiden.bc.model.SubscriptionStatus;

@Repository
public interface SubscriptionRepository extends JpaRepository<Subscription, Integer>{
  boolean existsByUser_AddressAndApproverAddressAndDevice_AddressAndStatus(String applicantAddress, String approverAddress, String deviceAddress, SubscriptionStatus status);
  
  Page<Subscription> findByUser_AddressAndStatusAndEndedAtLessThan(String applicantAddress, SubscriptionStatus status, Long todayTimestamp, Pageable pageable);

  Page<Subscription> findByApproverAddressAndStatus(String approverAddress, SubscriptionStatus status, Pageable pageable);
}





