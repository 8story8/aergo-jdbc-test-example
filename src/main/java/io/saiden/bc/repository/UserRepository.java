package io.saiden.bc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import io.saiden.bc.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String>{
  boolean existsByName(String name);
}
