package io.saiden.bc.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import io.saiden.bc.model.Plan;

@Repository
public interface PlanRepository extends JpaRepository<Plan, Integer>{
}
