package io.saiden.bc.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import io.saiden.bc.model.Device;

@Repository
public interface DeviceRepository extends JpaRepository<Device, String>{
  List<Device> findByUser_AddressNot(String address);
  Page<Device> findByUser_Address(String address, Pageable pageable);
}
