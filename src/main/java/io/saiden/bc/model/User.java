package io.saiden.bc.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Entity
@Data
@Table(name = "USER")
public class User {

  @Id
  @Column(name = "USER_ADDRESS")
  private String address;
  
  @Column(name = "USER_NAME")
  private String name;
  
  @Column(name = "USER_ASSET")
  private BigDecimal asset;
  
  @Column(name = "CREATED_AT")
  private Long createdAt;
  
  @OneToMany(mappedBy = "user")
  @JsonIgnore
  private List<Device> devices = new ArrayList<Device>();
  
  @OneToMany(mappedBy = "user")
  @JsonIgnore
  private List<Subscription> subscriptions = new ArrayList<Subscription>();
}
