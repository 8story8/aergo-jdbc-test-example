package io.saiden.bc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "DEVICE_DATA")
public class DeviceData {

  @Id
  @Column(name = "DEVICE_DATA_ID")
  private Integer id;
  
  @Column(name = "CREATED_AT")
  private Long createdAt;
  
  @ManyToOne
  @JoinColumn(name = "DEVICE_ADDRESS")
  private Device device;
}