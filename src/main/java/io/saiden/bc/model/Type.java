package io.saiden.bc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "TYPE")
public class Type {
  @Id
  @Column(name = "TYPE_ID")
  private Integer id;
  
  @Column(name = "TYPE_NAME")
  private String name;
  
  @OneToOne
  @JoinColumn(name = "PLAN_ID")
  private Plan plan;
}
