package io.saiden.bc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "SUBSCRIPTION")
public class Subscription {
  @Id
  @Column(name = "SUBSCRIPTION_ID")
  private Integer id;
 
  @Column(name = "STARTED_AT")
  private Long startedAt; 
  
  @Column(name = "ENDED_AT")
  private Long endedAt;
  
  @Column(name = "APPROVER_ADDRESS")
  private String approverAddress;
  
  @Column(name = "TRANSACTION_ID")
  private String transactionId;
  
  @ManyToOne
  @JoinColumn(name = "APPLICANT_ADDRESS")
  private User user;
  
  @Column(name = "SUBSCRIPTION_STATUS")
  @Enumerated(EnumType.STRING)
  private SubscriptionStatus status;
  
  @ManyToOne
  @JoinColumn(name = "DEVICE_ADDRESS")
  private Device device;
  
  @ManyToOne
  @JoinColumn(name = "PLAN_ID")
  private Plan plan;
}
