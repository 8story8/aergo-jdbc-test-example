package io.saiden.bc.model;

public enum SubscriptionStatus {
  PENDING,
  APPROVED
}
