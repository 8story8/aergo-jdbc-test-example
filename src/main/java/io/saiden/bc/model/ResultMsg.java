package io.saiden.bc.model;

import lombok.Data;

@Data
public class ResultMsg {

  Object data;
  boolean status;

  private ResultMsg(Object data, boolean status) {
    this.data = data;
    this.status = status;
  }

  public static ResultMsg of(Object data) {
    return new ResultMsg(data, true);
  }

  public static ResultMsg of(Object data, boolean status) {
    return new ResultMsg(data, status);
  }
}
