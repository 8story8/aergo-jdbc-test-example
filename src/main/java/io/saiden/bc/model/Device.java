package io.saiden.bc.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Entity
@Data
@Table(name = "DEVICE")
public class Device {

  @Id
  @Column(name = "DEVICE_ADDRESS")
  private String address;
  
  @Column(name = "CREATED_AT")
  private Long createdAt;
  
  @ManyToOne
  @JoinColumn(name = "USER_ADDRESS")
  private User user;
  
  @ManyToOne
  @JoinColumn(name = "TYPE_ID")
  private Type type;
  
  @OneToMany(mappedBy = "device", fetch = FetchType.EAGER)
  @JsonIgnore
  private List<Subscription> subscriptions = new ArrayList<Subscription>();
}