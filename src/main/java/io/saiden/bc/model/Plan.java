package io.saiden.bc.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name = "PLAN")
public class Plan {

  @Id
  @Column(name = "PLAN_ID")
  private Integer id;
  
  @Column(name = "PLAN_ASSET")
  private BigDecimal asset;
  
  @Column(name = "PLAN_REWARD")
  private BigDecimal reward;
  
  @Column(name = "PLAN_PERIOD")
  private Integer period;
}

