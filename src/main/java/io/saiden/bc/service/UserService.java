package io.saiden.bc.service;

import org.springframework.data.domain.Page;
import io.saiden.bc.dto.AvailableSubscription;
import io.saiden.bc.dto.CompletedSubscription;
import io.saiden.bc.dto.DeviceAndUser;
import io.saiden.bc.dto.PendingSubscription;
import io.saiden.bc.dto.ToBeCompletedSubscription;
import io.saiden.bc.model.User;

public interface UserService {
  boolean registerUserConditional(String address);
  User findUserByUserAddress(String address);
  Page<CompletedSubscription> findCompletedSubscriptionsByUserAddress(String address, Integer pageNum);
  Page<ToBeCompletedSubscription> findToBeCompletedSubscriptionsByUserAddress(String address, Integer pageNum);
  Page<PendingSubscription> findPendingSubscriptionsByUserAddress(String address,
      Integer pageNum);
  Page<AvailableSubscription> findAvailableSubscriptionsByUserAddress(String address,
      Integer pageNum);
  Page<AvailableSubscription> filterAvailableSubscriptionsByUserName(String address, String name,
      Integer pageNum);
  Page<DeviceAndUser> findDevicesByUserAddress(String address, Integer pageNum);
}
