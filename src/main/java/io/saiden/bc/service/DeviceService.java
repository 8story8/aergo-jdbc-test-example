package io.saiden.bc.service;

import java.util.List;
import org.springframework.data.domain.Page;
import io.saiden.bc.model.Device;


public interface DeviceService {
  List<Device> findRawAvailableDevicesByUserAddress(String address);
  Page<Device> findAvailableDevicesByUserAddress(String address, Integer pageNum);
  Page<Device> filterAvailableDevicesByUserName(String address, String name, Integer pageNum);
  Page<Device> findDevicesByUserAddress(String address, Integer pageNum);
}
