package io.saiden.bc.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import io.saiden.bc.dto.AvailableSubscription;
import io.saiden.bc.dto.CompletedSubscription;
import io.saiden.bc.dto.DeviceAndUser;
import io.saiden.bc.dto.PendingSubscription;
import io.saiden.bc.dto.ToBeCompletedSubscription;
import io.saiden.bc.model.Device;
import io.saiden.bc.model.Subscription;
import io.saiden.bc.model.SubscriptionStatus;
import io.saiden.bc.model.User;
import io.saiden.bc.repository.UserRepository;
import io.saiden.bc.service.DeviceService;
import io.saiden.bc.service.SubscriptionService;
import io.saiden.bc.service.UserService;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private DeviceService deviceService;

  @Autowired
  private SubscriptionService subscriptionService;


  public User findUserByUserAddress(String address) {
    return userRepository.findById(address)
        .orElseThrow(() -> new UsernameNotFoundException(address));
  }

  @Override
  public boolean registerUserConditional(String address) {
    boolean flag = userRepository.existsById(address);

    if (flag) {
      return false;
    }


    return true;
  }

  @Override
  public Page<CompletedSubscription> findCompletedSubscriptionsByUserAddress(String address,
      Integer pageNum) {
    Page<Subscription> pages =
        subscriptionService.findSubscriptionsByApplicantAddressAndSubscriptionStatus(address,
            SubscriptionStatus.APPROVED, pageNum);
    Page<CompletedSubscription> dtoPages = pages.map(CompletedSubscription::fromEntity);
    return dtoPages;
  }

  @Override
  public Page<ToBeCompletedSubscription> findToBeCompletedSubscriptionsByUserAddress(String address,
      Integer pageNum) {
    Page<Subscription> pages =
        subscriptionService.findSubscriptionsByApproverAddressAndSubscriptionStatus(address,
            SubscriptionStatus.PENDING, pageNum);
    Page<ToBeCompletedSubscription> dtoPages = pages.map(ToBeCompletedSubscription::fromEntity);
    return dtoPages;
  }

  @Override
  public Page<PendingSubscription> findPendingSubscriptionsByUserAddress(String address,
      Integer pageNum) {
    Page<Subscription> pages =
        subscriptionService.findSubscriptionsByApplicantAddressAndSubscriptionStatus(address,
            SubscriptionStatus.PENDING, pageNum);
    Page<PendingSubscription> dtoPages = pages.map(PendingSubscription::fromEntity);
    return dtoPages;
  }

  @Override
  public Page<AvailableSubscription> findAvailableSubscriptionsByUserAddress(String address,
      Integer pageNum) {
    Page<Device> pages = deviceService.findAvailableDevicesByUserAddress(address, pageNum);
    Page<AvailableSubscription> dtoPages = pages.map(AvailableSubscription::fromEntity);
    return dtoPages;
  }
  
  @Override
  public Page<AvailableSubscription> filterAvailableSubscriptionsByUserName(String address, String name,
      Integer pageNum) {
    Page<Device> pages = deviceService.filterAvailableDevicesByUserName(address, name, pageNum);
    Page<AvailableSubscription> dtoPages = pages.map(AvailableSubscription::fromEntity);
    return dtoPages;
  }

  @Override
  public Page<DeviceAndUser> findDevicesByUserAddress(String address, Integer pageNum) {
    Page<Device> pages = deviceService.findDevicesByUserAddress(address, pageNum);
    Page<DeviceAndUser> dtoPages = pages.map(DeviceAndUser::fromEntity);
    return dtoPages;
  }
}
