package io.saiden.bc.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import io.saiden.bc.model.Device;
import io.saiden.bc.model.Subscription;
import io.saiden.bc.repository.DeviceRepository;
import io.saiden.bc.service.DeviceService;

@Service
public class DeviceServiceImpl implements DeviceService {

  @Autowired
  private DeviceRepository deviceRepository;

  @Override
  public Page<Device> findAvailableDevicesByUserAddress(String address, Integer pageNum) {
    List<Device> availableDevices = findRawAvailableDevicesByUserAddress(address);
    
    PageRequest pageRequest = PageRequest.of(pageNum-1, 10);
    
    int start = Math.toIntExact(pageRequest.getOffset());
    int end = Math.min((start + pageRequest.getPageSize()), availableDevices.size());
    
    List<Device> output = new ArrayList<Device>();
    
    if(start <= end) {
      output = availableDevices.subList(start, end);
    }
    
    return new PageImpl<Device>(output, pageRequest, availableDevices.size());
  }
  
  @Override
  public Page<Device> filterAvailableDevicesByUserName(String address, String name, Integer pageNum) {
    List<Device> availableDevices = findRawAvailableDevicesByUserAddress(address);
    List<Device> filteredDevices = new ArrayList<Device>();
    
    for(Device device : availableDevices) {
      if(device.getUser().getName().equals(name)) {
        filteredDevices.add(device);
      }
    }
    
    PageRequest pageRequest = PageRequest.of(pageNum-1, 10);
    
    int start = Math.toIntExact(pageRequest.getOffset());
    int end = Math.min((start + pageRequest.getPageSize()), availableDevices.size());
    
    List<Device> output = new ArrayList<Device>();
    
    if(start <= end) {
      output = filteredDevices.subList(start, end);
    }
    
    return new PageImpl<Device>(output, pageRequest, availableDevices.size());
  }

  @Override
  public Page<Device> findDevicesByUserAddress(String address, Integer pageNum) {
    return deviceRepository.findByUser_Address(address, PageRequest.of(pageNum-1, 10, Sort.by(Direction.DESC, "createdAt")));
  }
  
  @Override
  public List<Device> findRawAvailableDevicesByUserAddress(String address) {
    List<Device> devices = deviceRepository.findByUser_AddressNot(address);
    List<Device> availableDevices = new ArrayList<Device>();
    for (Device device : devices) {
      boolean isAvailable = true;
      for (Subscription subscription : device.getSubscriptions()) {
        if (subscription.getUser().getAddress().equals(address)) {
          isAvailable = false;
        }
      }
      if (isAvailable) {
        availableDevices.add(device);
      }
    }
    
    availableDevices.sort(Comparator.comparing(Device::getCreatedAt).reversed());
    
    return availableDevices;
  }
}
