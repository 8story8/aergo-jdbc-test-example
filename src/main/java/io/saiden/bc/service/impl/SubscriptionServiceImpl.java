package io.saiden.bc.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import io.saiden.bc.model.Subscription;
import io.saiden.bc.model.SubscriptionStatus;
import io.saiden.bc.repository.SubscriptionRepository;
import io.saiden.bc.service.SubscriptionService;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {
  
  @Autowired
  private SubscriptionRepository subscriptionRepository;

  @Override
  public Page<Subscription> findSubscriptionsByApplicantAddressAndSubscriptionStatus(String address,
      SubscriptionStatus status, Integer pageNum) {
    LocalDateTime now = LocalDateTime.now();
    LocalDateTime processedNow = LocalDateTime.of(now.getYear(),
        now.getMonth(), now.getDayOfMonth(), 0, 0);
    Long todayTimestamp = Timestamp.valueOf(processedNow).getTime() / 1000;
    return subscriptionRepository.findByUser_AddressAndStatusAndEndedAtLessThan(address, status, todayTimestamp, PageRequest.of(pageNum-1, 10, Sort.by("endedAt")));
  }

  @Override
  public Page<Subscription> findSubscriptionsByApproverAddressAndSubscriptionStatus(String address,
      SubscriptionStatus status, Integer pageNum) {
    return subscriptionRepository.findByApproverAddressAndStatus(address, status, PageRequest.of(pageNum-1, 10, Sort.by(Direction.DESC, "device.createdAt")));
  }
}
