package io.saiden.bc.service;

import org.springframework.data.domain.Page;
import io.saiden.bc.model.Subscription;
import io.saiden.bc.model.SubscriptionStatus;

public interface SubscriptionService {
  Page<Subscription> findSubscriptionsByApplicantAddressAndSubscriptionStatus(String address, SubscriptionStatus status, Integer pageNum);

  Page<Subscription> findSubscriptionsByApproverAddressAndSubscriptionStatus(String address,
      SubscriptionStatus status, Integer pageNum);
}
