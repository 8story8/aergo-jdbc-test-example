package io.saiden.exec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"io.saiden.*"})
public class ServerLauncher {
  public static void main(String[] args) { 
    SpringApplication.run(ServerLauncher.class, args);
  }
}
