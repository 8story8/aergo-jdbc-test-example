package io.saiden.config;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "dbEntityManager",
    transactionManagerRef = "dbTransactionManager", basePackages = "io.saiden.repository")
public class DBSourceConfig {
  
  @Value("${datasource.database.dialect}")
  private String dialect;
  
  @Value("${datasource.database.ddl-auto}")
  private String ddlAuto;
  
  @Value("#{new Boolean('${datasource.database.show-sql}')}")
  private boolean showSql;
  
  @Primary
  @Bean("dbSourceProperties")
  @ConfigurationProperties("datasource.database")
  public DataSourceProperties dbSourceProperties() throws ClassNotFoundException {
    return new DataSourceProperties();
  }

  @Primary
  @Bean("dbSource")
  public DataSource dbSource(
      @Qualifier("dbSourceProperties") DataSourceProperties dbSourceProperties) throws ClassNotFoundException {
    return dbSourceProperties.initializeDataSourceBuilder().build();
  }

  @Primary
  @Bean("dbEntityManager")
  public LocalContainerEntityManagerFactoryBean dbEntityManager(
      EntityManagerFactoryBuilder builder, @Qualifier("dbSource") DataSource dbSource)
      throws SQLException {
      Map<String, Object> properties = new HashMap<String, Object>();
      properties.put("hibernate.dialect", dialect);
      properties.put("hibernate.hbm2ddl.auto", ddlAuto);
      properties.put("hibernate.show_sql", showSql);
    return builder.dataSource(dbSource).packages("io.saiden.model").persistenceUnit("db").properties(properties).build();
  }
  
  @Primary
  @Bean("dbTransactionManager")
  public JpaTransactionManager dbTransactionManager(@Qualifier("dbEntityManager") EntityManagerFactory dbEntityManager){
      JpaTransactionManager transactionManager = new JpaTransactionManager();
      transactionManager.setEntityManagerFactory(dbEntityManager);
      return transactionManager;
  }
}
