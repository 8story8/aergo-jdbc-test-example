package io.saiden.config;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "bcEntityManager",
transactionManagerRef = "bcTransactionManager", basePackages = "io.saiden.bc.repository")
public class BCSourceConfig  {
  
  @Value("${datasource.blockchain.dialect}")
  private String dialect;
  
  @Value("${datasource.blockchain.ddl-auto}")
  private String ddlAuto;
  
  @Value("#{new Boolean('${datasource.blockchain.show-sql}')}")
  private boolean showSql;
  
  @Bean
  @ConfigurationProperties("datasource.blockchain")
  public DataSourceProperties bcSourceProperties() throws ClassNotFoundException {
    return new DataSourceProperties();
  }

  @Bean(name = "bcSource")
  public DataSource bcSource(
      @Qualifier("bcSourceProperties") DataSourceProperties bcSourceProperties) throws ClassNotFoundException {
    return bcSourceProperties.initializeDataSourceBuilder().build();
  }

  @Bean(name = "bcEntityManager")
  public LocalContainerEntityManagerFactoryBean bcEntityManager(
      EntityManagerFactoryBuilder builder, @Qualifier("bcSource") DataSource bcSource)
      throws SQLException {
      Map<String, Object> properties = new HashMap<String, Object>();
      properties.put("hibernate.dialect", dialect);
      properties.put("hibernate.hbm2ddl.auto", ddlAuto);
      properties.put("hibernate.show_sql", showSql);
    return builder.dataSource(bcSource).packages(Jsr310JpaConverters.class).packages("io.saiden.bc.model").persistenceUnit("bc").properties(properties).build();
  }
  
  @Bean(name = "bcTransactionManager")
  public JpaTransactionManager bcTransactionManager(@Qualifier("bcEntityManager") EntityManagerFactory bcEntityManager){
      JpaTransactionManager transactionManager = new JpaTransactionManager();
      transactionManager.setEntityManagerFactory(bcEntityManager);
      return transactionManager;
  }
}
